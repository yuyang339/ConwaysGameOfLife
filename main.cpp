#include "stdafx.h"
#include "game.h"
#include <vector>
#include <string>
#include <chrono>
#include <thread>
#include <iostream>
#include <string>
#include <vector>
#include<cstdlib>
#include <assert.h>

int main() {
	std::unordered_set<Cell> blinker;
	blinker.emplace(-1, 0);
	blinker.emplace(0, 0);
	blinker.emplace(1, 0);
	std::unordered_set<Cell> expected;
	expected.emplace(0, -1);
	expected.emplace(0, 0);
	expected.emplace(0, 1);
	Game test_game(blinker);

	std::unordered_set<Cell>& universe = test_game.Run();
	
	for (auto iter = universe.begin(); iter != universe.end(); ++iter){
		for (auto exp_iter = expected.begin(); exp_iter != expected.end(); ++exp_iter) {
			if (iter->get_x() == exp_iter->get_x() && iter->get_y() == exp_iter->get_y())
			{
				std::cout << "match! " << std::endl;
				std::cout << "output x: " << iter->get_x() << " expected x: " << exp_iter->get_x() << std::endl;
				std::cout << "output y: " << iter->get_y() << " expected y: " << exp_iter->get_y() << std::endl;
				break;
			}
			
		}
	}
	
	universe = test_game.Run();
	swap(blinker, expected);
	for (auto iter = universe.begin(); iter != universe.end(); ++iter) {
		for (auto exp_iter = expected.begin(); exp_iter != expected.end(); ++exp_iter) {
			if (iter->get_x() == exp_iter->get_x() && iter->get_y() == exp_iter->get_y())
			{
				std::cout << "match! " << std::endl;
				std::cout << "output x: " << iter->get_x() << " expected x: " << exp_iter->get_x() << std::endl;
				std::cout << "output y: " << iter->get_y() << " expected y: " << exp_iter->get_y() << std::endl;
				break;
			}

		}
	}
	getchar();
}

