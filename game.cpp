

#include "stdafx.h"

#include "game.h"
#include <vector>
#include <iostream>
#include <algorithm>

Cell& Cell::operator=(const Cell& cell) {
	x = cell.get_x();
	y = cell.get_y();
	return *this;
}
bool Cell::operator==(const Cell& cell) const {
	if (x == cell.get_x() && y == cell.get_y())
		return true;
	else
		return false;
}
bool Cell::operator<(const Cell& cell) const {
	if (x < cell.get_x())
		return true;
	else if (x == cell.get_x()) {
		return y < cell.get_y();
	}
	else
		return false;
}
int Cell::get_x() const {
	return x;
}

int Cell::get_y() const {
	return y;
}
void Cell::set_x(int x) {
	this->x = x;
}

void Cell::set_y(int y) {
	this->y = y;
}

int Cell::get_id() const {
	return (int)this;
}

std::unordered_set<Cell>& Game::Run() {
	Evolv();
	return universe1;
}

// when we check the neighbors of a cell, we also need to check the neighbors of each neighbors of that cell. The reason is that we need to check whether a dead cell will become alive.
void Game::Check_alive(Cell cell) {
	std::unordered_set<Cell> nei_cells;
	int x = cell.get_x();
	int y = cell.get_y();
	int ori_x = x;
	int ori_y = y;
 	int cnt = 0;

	for (int i = 0; i < NUM_NEIGHBOURS; ++i) {
		Cell nei_cell;
		int offset_x = neighbours[i].first;
		int offset_y = neighbours[i].second;
		x += offset_x;
		y += offset_y;
		nei_cell.set_x(x);
		nei_cell.set_y(y);
		x = ori_x;
		y = ori_y;
		for (auto& c : universe1) {
			if (nei_cell == c) {
				cnt++;
			}
			else {
				if (nei_cells.empty())
					nei_cells.insert(nei_cell);
				else {
					for (auto& nc : nei_cells) {
						if (nc == nei_cell)
							continue;
						else
							nei_cells.insert(nei_cell);
					}
				}
			}
		}
	}

	// check whethe the neighbour cells will be alive in the next generation
	if (is_alive[cnt]) {
		universe2.insert(cell);
		
	}

	for (auto nc : nei_cells) {
		cnt = 0;
		int nei_x = nc.get_x();
		int nei_y = nc.get_y();
		ori_x = nei_x;
		ori_y = nei_y;
		for (int i = 0; i < NUM_NEIGHBOURS; ++i) {
			Cell nei_cell;
			int offset_x = neighbours[i].first;
			int offset_y = neighbours[i].second;
			nei_x += offset_x;
			nei_y += offset_y;
			nei_cell.set_x(nei_x);
			nei_cell.set_y(nei_y);
			nei_x = ori_x;
			nei_y = ori_y;
			for (auto& c : universe1) {
				if (nei_cell == c) {
					cnt++;
				}
			}
		}
		if (cnt == 3) {
			universe2.insert(nc);
		}
	}
	
	return;
}

void Game::Evolv() {

	universe2.clear();
	for (auto iter = universe1.begin(); iter != universe1.end(); iter++) {
		Check_alive(*iter);
	}

	std::swap(universe1, universe2);
}