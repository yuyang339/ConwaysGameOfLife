#pragma once
#include <vector>
#include <string>
#include <unordered_set>
#include <utility>

#define DEAD false
#define ALIVE true
#define NUM_NEIGHBOURS 8

class Cell {
private:
    // x, y represent the position of a cell in a universe
	int x, y;
public:
	Cell() = default;
	Cell(int x, int y) : x(x), y(y) {

	}

	Cell& operator=(const Cell& cell);
	bool operator==(const Cell& cell) const;
	bool operator<(const Cell& cell) const;
	int get_x() const;
	int get_y() const;
	void set_x(int x);
	void set_y(int y);
	int get_id() const;
};
namespace std {
	template <> struct hash<Cell>
	{
		size_t operator()(const Cell& x) const
		{
			return hash<int>()(x.get_id());
		}
	};
}

class Game {
private:
    // universe only contains live cells. dead cells will be removed from the universe.
	// the center of the universe is (0, 0).
	// the positions of cells are relative to (0, 0), e.g. (-1, -1), (2, 3), etc.
	std::unordered_set<Cell> universe1;
	std::unordered_set<Cell> universe2;
	std::pair<int, int> neighbours[NUM_NEIGHBOURS] = {
		{ -1, -1 },{ -1, 0 },{ -1, +1 },
		{ 0, -1 },           { 0, +1 },
		{ +1, -1 },{ +1, 0 },{ +1, +1 } };
	std::vector<bool> is_alive{ DEAD, DEAD, ALIVE, ALIVE, DEAD, DEAD, DEAD, DEAD, DEAD };
	
	void Check_alive(Cell cell);
	void Evolv();
public:
	Game(std::unordered_set<Cell>& input) : universe1(input) {

	}
	std::unordered_set<Cell>& Run();
};




